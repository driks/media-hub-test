
_pour recuperer le projet : git clone https://driks@bitbucket.org/driks/media-hub-test.git_

1) **lancer rabbitmq sur docker :**
A la racine du projet lancer `docker-compose up -d`
Si vous docker n'est pas present sur votre machine vous devrez telecharger Rabbitmq...

2) Creer une queue de nom : `original_title` c'est la queue dans laquelle les titres de film seront envoyés

2) Une fois le projet cloné et le Rabbitmq opérationnel, récupérer ce fichier ===>
https://datasets.imdbws.com/title.basics.tsv.gz

3) Vous pouvez à présent récuperer les dependances et librairies du projet...


**Envoyer les titres demandés dans la queue via `Sender.scala`:** 
pour cela il vous suffit de run le fichier et suivre les instructions de la console.


 **Recevoir les titres demandés dans la queue via `Receiver.scala`:** 
 pour cela il vous suffit de run le fichier et suivre les instructions de la console...
  
 