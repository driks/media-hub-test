package media

import scala.io.StdIn.readInt
import akka.stream.{ActorMaterializer, _}
import akka.stream.scaladsl._
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.alpakka.amqp.{AmqpUriConnectionProvider, NamedQueueSourceSettings, ReadResult}
import akka.stream.alpakka.amqp.scaladsl.AmqpSource

import scala.concurrent.{ExecutionContext, Future}
import scala.collection.immutable
import scala.util.{Failure, Success}

object Receive extends App {

  print("How many messages would you like to consume? (PS: if the number entered is bigger than the messages in queue, the process will wait until it consumes all the required messages) :");
  val size = readInt()

  implicit val system: ActorSystem = ActorSystem("media-hub-test")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext = ExecutionContext.global

  val dsn = "amqp://guest:guest@localhost:5672/%2f"
  val connectionProvider = AmqpUriConnectionProvider(dsn)


  private val action = new MessageReceiver


 action.result(materializer, connectionProvider, size)
   .andThen {
     case Success(result) => {
       result.foreach(f => println(f.bytes.utf8String))
       println(size + " messages had been received successfully")
     }
     case Failure(e) => e.printStackTrace()
   }
   .onComplete({ _ =>
     println("finished with success")
     system.terminate()
   })
}

class MessageReceiver {

  /**
   *
   * @param materializer : ActorMaterializer
   * @param connectionProvider : AmqpUriConnectionProvider
   * @param size : Long
   * @return
   */
  def result(implicit materializer : ActorMaterializer, connectionProvider : AmqpUriConnectionProvider, size : Long): Future[immutable.Seq[ReadResult]] = {
    val amqpSource: Source[ReadResult, NotUsed] =
      AmqpSource.atMostOnceSource(
        NamedQueueSourceSettings(connectionProvider, "original_title")
          .withAckRequired(false),
        bufferSize = 10
      )

    amqpSource
      .take(size)
      .runWith(Sink.seq)
  }
}
