package media


import java.io.{BufferedReader, FileInputStream, FileNotFoundException, InputStreamReader}
import java.util.zip.GZIPInputStream

import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success}
import scala.io.StdIn.readLine
import scala.concurrent._


import akka.stream.alpakka.amqp.{AmqpUriConnectionProvider, AmqpWriteSettings}
import akka.stream.alpakka.amqp.scaladsl.AmqpSink
import akka.stream.{ActorMaterializer, _}
import akka.stream.scaladsl._
import akka.Done
import akka.actor.ActorSystem
import akka.util.ByteString

object Sender extends App {

  val dsn = "amqp://guest:guest@localhost:5672/%2f"

  implicit val system: ActorSystem = ActorSystem("media-hub-test")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext = ExecutionContext.global


  val connectionProvider = AmqpUriConnectionProvider(dsn)
  private val action = new MessageSender

  print("please enter the path of your file to treat (e.g : /home/user/downloads/title.basics.tsv.gz) : ")
  val path = readLine();

  var titleList = (new ListBuffer[String]()).toList
  try {
    titleList = (new FileTreatment()).getTitleList(path)
  } catch {
    case _: FileNotFoundException => println("File has not been found. Try again")
    case _: Throwable => println("Something went wrong during your file treatment, make sur to pass 'title.basics.tsv.gz'. Try again")
  }


  action.send(materializer, connectionProvider, titleList)
    .andThen {
      case Success(_) => {
        println(titleList.length + " titre originaux envoyé(s) en queue")
        system.terminate()
      }
      case Failure(e) => {
        println(e.printStackTrace())
        system.terminate()
      }
    }

}

class MessageSender {

  /**
   *
   * @param materializer : ActorMaterializer
   * @param connectionProvider : AmqpUriConnectionProvider
   * @param titleList : Future[Done]
   * @return Future[Done]
   */
  def send(implicit materializer : ActorMaterializer, connectionProvider : AmqpUriConnectionProvider, titleList : List[String]): Future[Done] = {
    val amqpSink: Sink[ByteString, Future[Done]] =
      AmqpSink.simple(
        AmqpWriteSettings(connectionProvider)
          .withRoutingKey("original_title")
      )

    Source(titleList)
      .map(s => ByteString(s))
      .runWith(amqpSink)
  }
}


class FileTreatment {

  /**
   *
   * @param filePath : String
   * @return List[String]
   */
  def getTitleList(filePath : String)  : List[String] = {
    val iterator = GzFileIterator(new java.io.File(filePath), "UTF-8")
    var titleList = new ListBuffer[String]()

    iterator.foreach(fields => {
      val arrayFields = fields.split("\t")
      val title = arrayFields(3);
      if (arrayFields(1).equalsIgnoreCase("movie") && (arrayFields(8).indexOf("Comedy") != -1)) {
        //      println(title + "-----" + arrayFields(1) + " ---- " + arrayFields(8))
        titleList += title
      }
    })

    titleList.toList
  }
}




class BufferedReaderIterator(reader: BufferedReader) extends Iterator[String] {
  override def hasNext(): Boolean = reader.ready
  override def next(): String = reader.readLine()
}


object GzFileIterator {
  /**
   *
   * @param file : java.io.File
   * @param encoding : String
   * @return
   */
  def apply(file: java.io.File, encoding: String): BufferedReaderIterator = {
    new BufferedReaderIterator(
      new BufferedReader(
        new InputStreamReader(
          new GZIPInputStream(
            new FileInputStream(file)), encoding)))
  }
}
