name := "media-hub-test"

version := "0.1"

scalaVersion := "2.13.0"
libraryDependencies += "com.lightbend.akka" %% "akka-stream-alpakka-amqp" % "1.1.1"
libraryDependencies += "org.slf4j" % "slf4j-jdk14" % "1.7.25"

classLoaderLayeringStrategy := ClassLoaderLayeringStrategy.AllLibraryJars


